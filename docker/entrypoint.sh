#!/bin/bash -e

export EDITOR=vim
export PATH=/home/user/tutoral:/home/user/tutorial/node_modules/bin/:$PATH
export __fish_prompt_hostname="$HOSTNAME"
export LC_ALL=C.UTF-8
export XDG_RUNTIME_DIR=/tmp/docker-user-runtime

mkdir -p /tmp/docker-user-runtime

cd /home/user

#
# BEGIN FISH CONFIGURATION
#

# minimal tmux configuration for FISH as default SHELL
cat > .tmux.conf << EOF
set -g default-command /usr/bin/fish
set -g default-shell /usr/bin/fish
EOF

# minimal screen configuration for FISH as default SHELL
cat > .screenrc << EOF
shell /usr/bin/fish
EOF

#
# END FISH CONFIGURATION
#

if [ -n "$1" ]; then
    exec "$@"
else
    fish
fi
