build-image:
	docker-compose -f docker/docker-compose.yml build tutorial
.PHONY: build-image

run-image:
	docker-compose -f docker/docker-compose.yml run tutorial
.PHONY: run-image

test: app
	npx hardhat test
.PHONY: test

app:
	npx hardhat compile
.PHONY: app
